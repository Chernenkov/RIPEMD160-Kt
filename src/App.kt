import core.RipeMD160
import utils.FileUtils

fun main(args: Array<String>) {
    print("Write input filename: ")
    val input = "" + readLine() // for String? to be passed
    val ripeMD = RipeMD160()
    val hashSum = ripeMD.getHashSum(FileUtils().fileToByte(input))
    FileUtils().writeToFile("Hash_sum.txt", hashSum)
}

//example_text
//89ed1c03de67d7fac98b46192a53466aec2c8dce

//example_image.jpg
//00f6622575454b4fda0bf8b9c0531ae723383ed9

//example_image_one_pixel_delta.jpg
//deffbfbd0eeccab5a2bc21820ef22c7dbf17c374

//empty text by original:   9c1185a5c5e9fc54612808977ee8f548b2258d31
//empty text current:       9c1185a5c5e9fc54612808977ee8f548b2258d31