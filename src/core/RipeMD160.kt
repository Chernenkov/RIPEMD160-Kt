package core

class RipeMD160 {
    private val R1 = intArrayOf(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15)
    private val R2 = intArrayOf(7, 4, 13, 1, 10, 6, 15, 3, 12, 0, 9, 5, 2, 14, 11, 8)
    private val R3 = intArrayOf(3, 10, 14, 4, 9, 15, 8, 1, 2, 7, 0, 6, 13, 11, 5, 12)
    private val R4 = intArrayOf(1, 9, 11, 10, 0, 8, 12, 4, 13, 3, 7, 15, 14, 5, 6, 2)
    private val R5 = intArrayOf(4, 0, 5, 9, 7, 12, 2, 10, 14, 1, 3, 8, 11, 6, 15, 13)

    private val R1_ = intArrayOf(5, 14, 7, 0, 9, 2, 11, 4, 13, 6, 15, 8, 1, 10, 3, 12)
    private val R2_ = intArrayOf(6, 11, 3, 7, 0, 13, 5, 10, 14, 15, 8, 12, 4, 9, 1, 2)
    private val R3_ = intArrayOf(15, 5, 1, 3, 7, 14, 6, 9, 11, 8, 12, 2, 10, 0, 4, 13)
    private val R4_ = intArrayOf(8, 6, 4, 1, 3, 11, 15, 0, 5, 12, 2, 13, 9, 7, 10, 14)
    private val R5_ = intArrayOf(12, 15, 10, 4, 1, 5, 8, 7, 6, 2, 13, 14, 0, 3, 9, 11)

    private val S1 = intArrayOf(11, 14, 15, 12, 5, 8, 7, 9, 11, 13, 14, 15, 6, 7, 9, 8)
    private val S2 = intArrayOf(7, 6, 8, 13, 11, 9, 7, 15, 7, 12, 15, 9, 11, 7, 13, 12)
    private val S3 = intArrayOf(11, 13, 6, 7, 14, 9, 13, 15, 14, 8, 13, 6, 5, 12, 7, 5)
    private val S4 = intArrayOf(11, 12, 14, 15, 14, 15, 9, 8, 9, 14, 5, 6, 8, 6, 5, 12)
    private val S5 = intArrayOf(9, 15, 5, 11, 6, 8, 13, 12, 5, 12, 13, 14, 11, 8, 5, 6)

    private val S1_ = intArrayOf(8, 9, 9, 11, 13, 15, 15, 5, 7, 7, 8, 11, 14, 14, 12, 6)
    private val S2_ = intArrayOf(9, 13, 15, 7, 12, 8, 9, 11, 7, 7, 12, 7, 6, 15, 13, 11)
    private val S3_ = intArrayOf(9, 7, 15, 11, 8, 6, 6, 14, 12, 13, 5, 14, 13, 13, 7, 5)
    private val S4_ = intArrayOf(15, 5, 8, 11, 14, 14, 6, 14, 6, 9, 12, 9, 12, 5, 15, 8)
    private val S5_ = intArrayOf(8, 5, 12, 9, 12, 5, 14, 6, 8, 13, 6, 5, 15, 13, 11, 11)

    private val h = intArrayOf(
            0x67452301,
            -0x10325477,
            -0x67452302,
            0x10325476,
            -0x3c2d1e10) // magic numbers


    /**
     * Fun casts data to 448 bits (mod 512).
     * Step 1. Block is filled with data.
     * Step 2. 1 is added if end of block reached.
     * Step 3. Fill with zeroes, if block was smaller and data is already written.
     * Step 4. Add message length to the result of Step 3.
     */
    private fun initData(data: ByteArray): ByteArray {
        val byteCount = data.size
        val bitCount = (byteCount * 8).toLong()
        // 56 bytes mod 64 === 448 bits mod 512
        val size = if (data.size % 64 + 1 <= 56) {
            data.size + (64 - data.size % 64)
        } else {
            data.size + 64 + (64 - data.size % 64)
        }
        val byteArray = ByteArray(size)
        for (i in byteArray.indices) {
            when {
                // filling with data, then 1s, then 0s by spec
                i < byteCount -> byteArray[i] = data[i]
                i == byteCount -> byteArray[i] = 0x80.toByte() // 0x80(hex) = 128(dec) = 10000000(bin)
                else -> byteArray[i] = 0x00
            }
        }
        // filling last 8 bytes (64 bits) with shifted bits length
        byteArray[byteArray.size - 8] = bitCount.toByte()
        byteArray[byteArray.size - 7] = (bitCount shr 8).toByte()
        byteArray[byteArray.size - 6] = (bitCount shr 16).toByte()
        byteArray[byteArray.size - 5] = (bitCount shr 24).toByte()
        byteArray[byteArray.size - 4] = (bitCount shr 32).toByte()
        byteArray[byteArray.size - 3] = (bitCount shr 40).toByte()
        byteArray[byteArray.size - 2] = (bitCount shr 48).toByte()
        byteArray[byteArray.size - 1] = (bitCount shr 56).toByte()
        return byteArray
    }

    /**
     * Fun splits input data [bytes] into array [data.size/64][16]
     * for S() and R() operations to use.
     */
    private fun getWords(bytes: ByteArray): Array<IntArray> {
        val words = Array(bytes.size / 64) { IntArray(16) }
        var id = 0
        for (i in 0 until bytes.size / 64) {
            for (j in 0..15) {
                // shift for 8 bits left each time for int
                words[i][j] = (bytes[id].toInt() and 0xff // the little bits
                        or (bytes[id + 1].toInt() and 0xff shl 8)
                        or (bytes[id + 2].toInt() and 0xff shl 16)
                        or (bytes[id + 3].toInt() and 0xff shl 24)) // the bigger bits
                id += 4 // to navigate through integers
            }
        }
        return words
    }
//
//    fun printBinaryFormat(str: String, bytes: Int): String {
//        val sb = StringBuilder()
//        for (i in str.length until bytes * 8) {
//            sb.append("0")
//        }
//        sb.append(str)
//        return sb.toString()
//    }

    /**
     * Fun counts hash sum for byte array [data].
     * Step 1. Init data with magic numbers array [h].
     * Step 2. Count major sum changing data.
     * Step 3. Count parallel sum changing data.
     * Step 4. Make last change of magic numbers array [h] using changed data.
     * Step 5. ???
     * PROFIT
     */
    fun getHashSum(data: ByteArray): ByteArray {
        val bytesData = initData(data)
        val words = getWords(bytesData) // TODO
        val size = bytesData.size / 64
        var A: Int
        var B: Int
        var C: Int
        var D: Int
        var E: Int
        var A_: Int // parallel
        var B_: Int
        var C_: Int
        var D_: Int
        var E_: Int
        for (i in 0 until size) { // run through byte array of data
            A = h[0] // set initial values from magic numbers
            B = h[1]
            C = h[2]
            D = h[3]
            E = h[4]
            A_ = h[0]
            B_ = h[1]
            C_ = h[2]
            D_ = h[3]
            E_ = h[4]
            var T: Int
            for (j in 0..79) {
                // the major sum
                val sum1 = A + getF(B, C, D, j) + words[i][getR(j)] + getK(j) // TODO
                T = Integer.rotateLeft(sum1, getS(j)) + E // TODO
                A = E
                E = D
                D = Integer.rotateLeft(C, 10)
                C = B
                B = T
                // the parallel sum
                val sum2 = A_ + getF(B_, C_, D_, 79 - j) + words[i][getR_(j)] + getK_(j)
                T = Integer.rotateLeft(sum2, getS_(j)) + E_
                A_ = E_
                E_ = D_
                D_ = Integer.rotateLeft(C_, 10)
                C_ = B_
                B_ = T
            }
            T = h[1] + C + D_
            h[1] = h[2] + D + E_
            h[2] = h[3] + E + A_
            h[3] = h[4] + A + B_
            h[4] = h[0] + B + C_
            h[0] = T
        }
        return getByteArray(h) // return the mutated array of magic int numbers as byte array
    }

    /**
     * Fun makes [Byte] array out of [Int] array
     */
    private fun getByteArray(h: IntArray): ByteArray {
        val byteArr = ByteArray(h.size * 4) // int = 4 bytes
        for (i in h.indices) {
            byteArr[4 * i    ] = h[i].toByte()
            byteArr[4 * i + 1] = (h[i] shr 8).toByte() // 8 shifting to right is for each byte
            byteArr[4 * i + 2] = (h[i] shr 16).toByte()
            byteArr[4 * i + 3] = (h[i] shr 24).toByte()
        }
        return byteArr
    }

    /**
     * Fun decides on which interval of steps the algorithm is at the moment.
     */
    private fun getInterval(j: Int): Int {
        return when (j) {
            in 0..15 -> 1
            in 16..31 -> 2
            in 32..47 -> 3
            in 48..63 -> 4
            in 64..79 -> 5
            else -> {
                println("Bad interval")
                -1
            }
        }
    }

    /**
     * Bit operations due to the current interval(number of step).
     */
    private fun getF(x: Int, y: Int, z: Int, j: Int): Int {
        when (getInterval(j)) {
            1 -> return x xor y xor z
            2 -> return x and y or (x.inv() and z)
            3 -> return x or y.inv() xor z
            4 -> return x and z or (y and z.inv())
            5 -> return x xor (y or z.inv())
        }
        return -1
    }

    /**
     * K() major
     * Magic numbers for major sum
     * due to the current interval(number of step).
     */
    private fun getK(j: Int): Int {
        when (getInterval(j)) {
            1 -> return 0x00000000
            2 -> return 0x5a827999
            3 -> return 0x6ed9eba1
            4 -> return -0x70e44324
            5 -> return -0x56ac02b2
        }
        return -1
    }

    /**
     * K()_ parallel
     * Magic numbers for parallel sum
     * due to the current interval(number of step).
     */
    private fun getK_(j: Int): Int {
        when (getInterval(j)) {
            1 -> return 0x50A28BE6
            2 -> return 0x5c4dd124
            3 -> return 0x6d703ef3
            4 -> return 0x7a6d76e9
            5 -> return 0x00000000
        }
        return -1
    }

    /**
     * R() major
     * Count numbers for major sum
     * due to the current interval(number of step).
     */
    private fun getR(j: Int): Int {
        when (getInterval(j)) {
            1 -> return R1[j]
            2 -> return R2[j % 16]
            3 -> return R3[j % 16]
            4 -> return R4[j % 16]
            5 -> return R5[j % 16]
        }
        return -1
    }

    /**
     * R_() parallel
     * Count numbers for parallel sum
     * due to the current interval(number of step).
     */
    private fun getR_(j: Int): Int {
        when (getInterval(j)) {
            1 -> return R1_[j % 16]
            2 -> return R2_[j % 16]
            3 -> return R3_[j % 16]
            4 -> return R4_[j % 16]
            5 -> return R5_[j % 16]
        }
        return -1
    }

    /**
     * S() major
     * Left rotation distances for major sum
     * due to the current interval(number of step).
     */
    private fun getS(j: Int): Int {
        when (getInterval(j)) {
            1 -> return S1[j % 16]
            2 -> return S2[j % 16]
            3 -> return S3[j % 16]
            4 -> return S4[j % 16]
            5 -> return S5[j % 16]
        }
        return -1
    }

    /**
     * S()_ parallel
     * Left rotation distances for parallel sum
     * due to the current interval(number of step).
     */
    private fun getS_(j: Int): Int {
        when (getInterval(j)) {
            1 -> return S1_[j % 16]
            2 -> return S2_[j % 16]
            3 -> return S3_[j % 16]
            4 -> return S4_[j % 16]
            5 -> return S5_[j % 16]
        }
        return -1
    }
}
