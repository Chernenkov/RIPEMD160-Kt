package utils

import java.io.*
import java.nio.file.Files

class FileUtils {

    fun fileToByte(filename: String): ByteArray
            = Files.readAllBytes(File(filename).toPath())

    fun writeToFile(filename: String, hashCode: ByteArray)
            = FileOutputStream(File(filename)).write(getByteArray(hashCode).toByteArray())

    private fun getByteArray(array: ByteArray): String {
        val buffer = StringBuilder()
        array.forEach {
            var strValue = Integer.toHexString(it.toInt() and 0xff)
            strValue = if(strValue.length == 1) ("0$strValue") else (strValue)
            buffer.append(strValue)
        }
        return buffer.toString()
    }
}
